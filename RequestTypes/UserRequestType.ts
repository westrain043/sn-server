import { ObjectType, Field, InputType } from "type-graphql";
import { GraphQLScalarType, Kind } from "graphql";

@ObjectType()
export class User {
  @Field()
  Id: number;
  @Field()
  FirstName: string;
  @Field()
  LastName: string;
  @Field()
  Avatar: string;
}

export const MixedUserType = new GraphQLScalarType({
  name: "MixedUserType",
  description: "MixedUserType",
  parseValue(value: Iam | Friend | User) {
    return value;
  },
  serialize(value: Iam | Friend | User) {
    return value;
  },
});

@ObjectType()
export class Friend {
  @Field()
  Id: number;
  @Field()
  FirstName: string;
  @Field()
  LastName: string;
  @Field()
  Avatar: string;
  @Field()
  DateOfBirth: Date;
}

@ObjectType()
export class Iam {
  @Field()
  Id: number;
  @Field()
  Login: string;
  @Field()
  FirstName: string;
  @Field()
  LastName: string;
  @Field()
  Avatar: string;
  @Field()
  DateOfBirth: Date;
  @Field((type) => [User], { nullable: true })
  Friends: [User];
  @Field()
  PrivateInfo: string;
}

@ObjectType()
export class UserRequest {
  @Field()
  Id: number;
  @Field()
  UserId: number;
  @Field()
  FriendId: number;
  @Field()
  StatusId: number;
}

@ObjectType()
export class UserAccessResponse {
  @Field()
  Token: string;
}

@InputType()
export class UserInput {
  @Field()
  FirstName: string;
  @Field()
  LastName: string;
  @Field()
  Avatar: string;
  @Field()
  DateOfBirth: Date;
  @Field({ nullable: true })
  PrivateInfo: string;
}

@InputType()
export class UserSignUpInput {
  @Field()
  Login: string;
  @Field()
  Password: string;
  @Field()
  FirstName: string;
  @Field()
  LastName: string;
  @Field({ nullable: true })
  Avatar: string;
  @Field()
  DateOfBirth: Date;
  @Field({ nullable: true })
  PrivateInfo: string;
}

@InputType()
export class UserSignInInput {
  @Field()
  Login: string;
  @Field()
  Password: string;
}
