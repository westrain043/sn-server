# sn-server

**Требования**: node, ts-node, typeorm, docker, sh

**1 Запуск** с docker hub:
<br>
в таком случаии будут загруженны 2 образа, один mysql, второй sn-servers сообственно программа,
<br>
а при запуске будут запущенны 3 контейнера mysql, sn-auth, sn-api.
<br>
So, для этого нужно выполнить команды:
<br>

**1.1** sh ./Setup/run.sh // сообственно всё и делает
<br>
**1.2** sh ./Setup/init.sh // подключается к sn-api и синхронизирует entity, а так же поссев данных


**2 Запуск** на локальном: 

желательно чтобы глобально стояли **ts-node**, **typeorm**, но если нехотим то: **npm i ts-node typeorm** или **tsc && node ./dist/** и так далее

**2.1** npm i
<br>
**2.2** ts-node AuthServer 
<br>
**2.3** ts-node ApiServer 


**Примеры запросов**:
<br>
Способ передачи запросов при помощи GraphQL схем
<br>
Схему можно посмотреть в [/Schemes](https://gitlab.com/westrain043/sn-server/-/tree/master/Schemes)
<br>
Для выполнение запросов на api server требуеться header authorization:Token и схема api.gql
<br>
Чтобы получить токен нужно авторизоваться на auth server'е при помощи схемы auth.gql

```
mutation {
    Register(user:{
        Avatar:"/Avatar",
        FirstName:"Ivan",
        LastName:"Ivanov",
        DateOfBirth:"01.01.1930",
        Login:"Login",
        Password:"Password"
        PrivateInfo:"Секретики"
    }) {
        Id
        FirstName
    }
}

{
    Login(user:{
        Login:"Login",Password:"Password"
    }) {
        Token
    }
}

{
    Users {
        Id
        FirstName
        LastName
    }
}

{
    User(Id:3) 
}

{
    Friends {
        Id
        Avatar
        FirstName
        LastName
        DateOfBirth
    }
}

mutation {
    AddFriend(Id:8) 
}

mutation {
    AcceptInvite(Id:IdUserFriendRequest) #указывается id запроса
}

mutation {
    RejectInvite(Id:IdUserFriendRequest) #указывается id запроса
}

mutation {
    DeleteFriend(Id:8)
}
```

