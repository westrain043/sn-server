export const AUTH_SERVER_PORT = process.env.AUTH_SERVER_PORT || 3131;
export const API_SERVER_PORT = process.env.API_SERVER_PORT || 3130;

export const UserRequestStatuses = {
  wait: {
    id: 1,
    name: "wait",
  },
  accepted: {
    id: 2,
    name: "accepted",
  },
  rejected: {
    id: 3,
    name: "rejected",
  },
};

export const database = {
  host: process.env.DB_HOST || "localhost",
  port: Number(process.env.PORT) || 3306,
  username: process.env.DB_USERNAME || "admin",
  password: process.env.DB_PASSWORD || "111",
  database: process.env.DB_NAME || "social-network-db",
};
