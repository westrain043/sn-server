import "reflect-metadata";
import { db } from "./Database/db";
import { UserEntity } from "./Database/Entity/UserEntity";
import * as express from "express";
import * as graphqlHTTP from "express-graphql";
import { buildSchema } from "type-graphql";
import { AccessEntity } from "./Database/Entity/AccessEntity";
import { json } from "body-parser";
import { optionsGQL } from "./Helpers/GeneralHelper";
import { FriendEntity } from "./Database/Entity/FriendEntity";
import { UserRequestStatusEntity } from "./Database/Entity/UserRequestStatusEntity";
import { UserRequestFriendsEntity } from "./Database/Entity/UserRequestFriendsEntity";
import { AccessResolver } from "./Resolvers/AccessResolver";
import * as cors from "cors";
import { AUTH_SERVER_PORT } from "./config";

db([
  UserEntity,
  UserRequestStatusEntity,
  FriendEntity,
  UserRequestFriendsEntity,
  AccessEntity,
]).then(async (connection) => {
  try {
    const schema = await buildSchema({
      resolvers: [AccessResolver],
      emitSchemaFile: "./Schemes/auth.gql",
    });

    const app = express();

    app.use(cors());
    app.use(json());

    app.use(
      "/auth",
      graphqlHTTP((req, res, params) => {
        console.log(params);

        return optionsGQL(schema, connection);
      })
    );

    app.listen(AUTH_SERVER_PORT);

    console.log(`POST http://localhost:${AUTH_SERVER_PORT}/auth`);
  } catch (error) {
    console.log(error.stack);
    process.exit();
  }
});
