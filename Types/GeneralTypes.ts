import { Connection } from "typeorm";
import { UserEntity } from "../Database/Entity/UserEntity";

export interface IContext {
  db: Connection;
  auth: {
    user: UserEntity;
    token: string;
  };
}
