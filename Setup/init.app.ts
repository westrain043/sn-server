
import { db } from "../Database/db";
import { UserRequestStatusEntity } from "../Database/Entity/UserRequestStatusEntity";
import { Connection } from "typeorm";
import { UserRequestStatuses } from "../config";
import { UserEntity } from "../Database/Entity/UserEntity";
import { FriendEntity } from "../Database/Entity/FriendEntity";
import { AccessEntity } from "../Database/Entity/AccessEntity";
import { UserRequestFriendsEntity } from "../Database/Entity/UserRequestFriendsEntity";

const f = (Id, Name) => {
  const i = new UserRequestStatusEntity();
  i.Id = Id;
  i.Name = Name;
  return i;
};

const init = async (c: Connection) => {
  for (const v of Object.values(UserRequestStatuses)) {
    await c.manager.save(f(v.id, v.name));
  }
};

db(
  [
    UserEntity,
    UserRequestStatusEntity,
    FriendEntity,
    UserRequestFriendsEntity,
    AccessEntity,
  ],
  [],
  true
).then(async (c) => {
  await init(c);
  await c.close();
});
