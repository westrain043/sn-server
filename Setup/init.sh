source $(dirname "$0")/vars.sh

docker exec $apiServer bash -c '
 node ./node_modules/typeorm/cli.js schema:drop &&
 node ./node_modules/typeorm/cli.js schema:sync &&
 node ./Setup/init.app.js &&
 node ./node_modules/typeorm-seeding/dist/cli.js seed'