source $(dirname "$0")/vars.sh

docker pull mysql
docker pull $repo:latest

docker rm -f $mysqlServerName
docker rm -f $authServer
docker rm -f $apiServer

docker run --name $mysqlServerName \
-p 3306:3306 \
-e MYSQL_DATABASE=$dbName \
-e MYSQL_USER=$dbUser \
-e MYSQL_PASSWORD=$dbPass \
-e MYSQL_ROOT_PASSWORD=$dbPass \
-d mysql:latest \
--default-authentication-plugin=mysql_native_password

docker run -d --restart always -p 3131:3131 --name $authServer -e DB_HOST=sn-mysql --link sn-mysql:sn-mysql $repo bash -c "node AuthServer"
docker run -d --restart always -p 3130:3130 --name $apiServer -e DB_HOST=sn-mysql --link sn-mysql:sn-mysql $repo bash -c "node ApiServer"

echo 'Подождите 50 сек, mysql брыкается:('
sleep 50