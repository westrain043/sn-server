source $(dirname "$0")/vars.sh
docker build -t $imageName:latest --file $(dirname "$0")/dockerfile .
docker tag $imageName $repo
docker push $repo