import "reflect-metadata";
import { db } from "./Database/db";
import { UserEntity } from "./Database/Entity/UserEntity";
import { UserRequestStatusEntity } from "./Database/Entity/UserRequestStatusEntity";
import { FriendEntity } from "./Database/Entity/FriendEntity";
import { UserRequestFriendsEntity } from "./Database/Entity/UserRequestFriendsEntity";
import * as express from "express";
import * as graphqlHTTP from "express-graphql";
import { buildSchema } from "type-graphql";
import { FriendResolver } from "./Resolvers/FriendResolver";
import { UserResolver } from "./Resolvers/UserResolver";
import { AccessEntity } from "./Database/Entity/AccessEntity";
import { json } from "body-parser";
import { optionsGQL } from "./Helpers/GeneralHelper";
import * as cors from "cors";
import { API_SERVER_PORT } from "./config";

db([
  UserEntity,
  UserRequestStatusEntity,
  FriendEntity,
  UserRequestFriendsEntity,
  AccessEntity,
]).then(async (connection) => {
  try {
    //например endpoint api:)
    const api = await buildSchema({
      resolvers: [FriendResolver, UserResolver],
      emitSchemaFile: "./Schemes/api.gql",
    });

    const app = express();

    app.use(cors());
    app.use(json());

    app.post(
      "/api",
      graphqlHTTP(async (request) => {

        const token = request.headers.authorization;

        const error = "Unauthorized";

        if (!token) throw new Error(error);

        let user = await connection.manager
          .createQueryBuilder(AccessEntity, "Access")
          .leftJoinAndSelect("Access.User", "U")
          .where("Access.Token = :at", { at: token })
          .getOne();

        if (!user) throw new Error(error);
        else return optionsGQL(api, connection, { user: user.User, token });
      })
    );

    app.listen(API_SERVER_PORT);

    console.log(`POST http://localhost:${API_SERVER_PORT}/api`);
  } catch (error) {
    console.log(error.stack);
    process.exit();
  }
});
