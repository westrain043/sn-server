import { Arg, Mutation, Resolver, Ctx, Query } from "type-graphql";
import { UserEntity } from "../Database/Entity/UserEntity";
import {
  User,
  UserInput,
  Friend,
  Iam,
  MixedUserType,
} from "../RequestTypes/UserRequestType";
import { IContext } from "../Types/GeneralTypes";
import {
  AuthUserFindAndDelete,
  AuthUserFindAndSave,
  isIamUser,
  existsUser,
} from "../Helpers/UserHelper";
import { FriendEntity } from "../Database/Entity/FriendEntity";

@Resolver()
export class UserResolver {
  constructor(@Ctx() ctx: IContext) {}

  @Query(() => [User])
  async Users(@Ctx() ctx: IContext) {
    return await ctx.db.manager
      .createQueryBuilder(UserEntity, "User")
      .getMany();
  }

  @Query((of) => MixedUserType)
  async User(@Arg("Id") Id: number, @Ctx() ctx: IContext) {
    const getUser = await existsUser(Id, ctx);   

    if (!getUser) throw new Error("Пользователь не найден!");

    if (isIamUser(Id, ctx)) {
      const user = new Iam();
      const {
        Avatar,
        FirstName,
        LastName,
        DateOfBirth,
        PrivateInfo,
        Login,
      } = getUser;
      user.Avatar = Avatar;
      user.LastName = LastName;
      user.FirstName = FirstName;
      user.DateOfBirth = DateOfBirth;
      user.PrivateInfo = PrivateInfo;
      user.Login = Login;
      return user;
    }
    const friend = await ctx.db.manager
      .createQueryBuilder(FriendEntity, "Friends")
      .leftJoinAndSelect("Friends.Friend", "Friend")
      .where("Friends.UserId = :uid", { uid: ctx.auth.user.Id })
      .andWhere("Friends.FriendId = :fid", { fid: Id })
      .getOne();

    if (!friend) {
      const user = new User();
      const { Avatar, FirstName, LastName } = getUser;
      user.Avatar = Avatar;
      user.FirstName = FirstName;
      user.LastName = LastName;      

      return user;
    } else {
      const user = new Friend();
      const { Avatar, DateOfBirth, FirstName, LastName } = friend.Friend;
      user.Avatar = Avatar;
      user.DateOfBirth = DateOfBirth;
      user.FirstName = FirstName;
      user.LastName = LastName;

      return user;
    }
  }

  @Mutation(() => User)
  async UpdateUser(@Arg("user") user: UserInput, @Ctx() ctx: IContext) {
    const instance = new UserEntity();
    instance.FirstName = user.FirstName;
    instance.LastName = user.LastName;
    instance.Avatar = user.Avatar;
    instance.DateOfBirth = user.DateOfBirth;
    instance.PrivateInfo = user.PrivateInfo;
    return await AuthUserFindAndSave(ctx, UserEntity.name, instance);
  }

  @Mutation(() => User)
  async DeleteUser(@Ctx() ctx: IContext) {
    return await AuthUserFindAndDelete(ctx, UserEntity.name);
  }
}
