import { Mutation, Resolver, Arg, Ctx, Query } from "type-graphql";
import { FriendEntity } from "../Database/Entity/FriendEntity";
import { UserRequestFriendsEntity } from "../Database/Entity/UserRequestFriendsEntity";
import { User, UserRequest, Friend } from "../RequestTypes/UserRequestType";
import { IContext } from "../Types/GeneralTypes";
import { UserRequestStatuses } from "../config";
import {
  existsFriend,
  existsRequest,
  existsUser,
  getInvite,
  isIamUser,
} from "../Helpers/UserHelper";

@Resolver()
export class FriendResolver {
  @Query(() => [Friend])
  async Friends(@Ctx() ctx: IContext) {
    return (
      await ctx.db.manager
        .createQueryBuilder(FriendEntity, "Friends")
        .leftJoinAndSelect("Friends.Friend", "Friend")
        .where("Friends.UserId = :uid", { uid: ctx.auth.user.Id })
        .getMany()
    ).map((e) => e.Friend);
  }

  @Mutation(() => String)
  async AddFriend(@Arg("Id") Id: number, @Ctx() ctx: IContext) {
    if (!(await existsUser(Id, ctx))) throw new Error("Пользователь не найден");
    if (await existsRequest(Id, ctx))
      throw new Error("Запрос был отправлен рание");
    if (await existsFriend(Id, ctx))
      throw new Error("Пользователь находиться в списке друзей");

    const ins = new UserRequestFriendsEntity();
    ins.User = ctx.auth.user;
    ins.FriendId = Id;
    ins.StatusId = UserRequestStatuses.wait.id;
    await ctx.db.manager.save(ins);
    return "Заявка отправленна";
  }

  @Mutation(() => String)
  async AcceptInvite(@Arg("Id") Id: number, @Ctx() ctx: IContext) {
    const inviteQuery = ctx.db.manager
      .createQueryBuilder(UserRequestFriendsEntity, "UserRequestFriends")
      .where("UserRequestFriends.FriendId = :fid", { fid: ctx.auth.user.Id })
      .andWhere("UserRequestFriends.Id = :id", { id: Id })
      .andWhere("UserRequestFriends.StatusId = :sid", {
        sid: UserRequestStatuses.wait.id,
      });

    const invite = await inviteQuery.getOne();

    if (invite) {
      const queryRunner = ctx.db.createQueryRunner();

      await queryRunner.startTransaction();

      try {
        const userRequestFriendsEntity = new UserRequestFriendsEntity();

        userRequestFriendsEntity.Id = Id;
        userRequestFriendsEntity.UserId = invite.UserId;
        userRequestFriendsEntity.FriendId = ctx.auth.user.Id;
        userRequestFriendsEntity.StatusId = UserRequestStatuses.accepted.id;

        const friendEntityFrom = new FriendEntity();
        friendEntityFrom.UserId = invite.UserId;
        friendEntityFrom.FriendId = ctx.auth.user.Id;

        const friendEntityTo = new FriendEntity();
        friendEntityTo.UserId = ctx.auth.user.Id;
        friendEntityTo.FriendId = invite.UserId;

        await queryRunner.manager.save(userRequestFriendsEntity);
        await queryRunner.manager.save(friendEntityFrom);
        await queryRunner.manager.save(friendEntityTo);

        return await queryRunner.commitTransaction();
      } catch (error) {
        await queryRunner.rollbackTransaction();
      } finally {
        await queryRunner.release();
      }

      return "Заявка одобрена";
    } else throw new Error("Запроса не существует");
  }

  @Mutation(() => String)
  async RejectInvite(@Arg("Id") Id: number, @Ctx() ctx: IContext) {
    const inviteQuery = ctx.db.manager
      .createQueryBuilder(UserRequestFriendsEntity, "UserRequestFriends")
      .where("UserRequestFriends.FriendId = :fid", { fid: ctx.auth.user.Id })
      .andWhere("UserRequestFriends.Id = :id", { id: Id })
      .andWhere("UserRequestFriends.StatusId = :sid", {
        sid: UserRequestStatuses.wait.id,
      });

    const invite = await inviteQuery.getOne();

    if (invite) {
      await inviteQuery
        .update({ StatusId: UserRequestStatuses.rejected.id })
        .execute();
      return "Заявка отправлена";
    } else throw new Error("Запроса не существует");
  }

  @Mutation(() => String)
  async DeleteFriend(@Arg("Id") Id: number, @Ctx() ctx: IContext) {
    const invite = await getInvite(Id, ctx);

    if (isIamUser(Id, ctx))
      throw new Error(
        "Пользователь не может удалить себя из собственного списка друзей"
      );

    const getRecordFriendUser = await ctx.db.manager
      .createQueryBuilder(FriendEntity, "Friends")
      .where("Friends.UserId = :uid", { uid: Id })
      .andWhere("Friends.FriendId = :fid", { fid: ctx.auth.user.Id })
      .getOne();

    const getRecordUserFriend = await ctx.db.manager
      .createQueryBuilder(FriendEntity, "Friends")
      .where("Friends.UserId = :uid", { uid: ctx.auth.user.Id })
      .andWhere("Friends.FriendId = :fid", { fid: Id })
      .getOne();

    if (!getRecordUserFriend)
      throw new Error("Указанный пользователь не является другом");

    const queryRunner = ctx.db.createQueryRunner();

    await queryRunner.startTransaction();
    try {
      await invite.action();
      await queryRunner.manager.delete(FriendEntity, {
        Id: getRecordFriendUser.Id,
      });
      await queryRunner.manager.delete(FriendEntity, {
        Id: getRecordUserFriend.Id,
      });

      await queryRunner.commitTransaction();

      return "Заявка отправлена";
    } catch (error) {
      await queryRunner.rollbackTransaction();
      throw new Error(error.message);
    } finally {
      await queryRunner.release();
    }
  }
}
