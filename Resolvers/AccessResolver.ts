import {
  Arg,
  Mutation,
  Resolver,
  Ctx,
  Query,
} from "type-graphql";
import { UserEntity } from "../Database/Entity/UserEntity";
import { sign } from "jsonwebtoken";

import {
  User,
  UserSignInInput,
  UserSignUpInput,
  UserAccessResponse,
} from "../RequestTypes/UserRequestType";
import { IContext } from "../Types/GeneralTypes";
import { AccessEntity } from "../Database/Entity/AccessEntity";
import { GetUserByLoginAndPassword } from "../Helpers/UserHelper";

@Resolver()
export class AccessResolver {
  @Query(() => UserAccessResponse)
  async Login(@Arg("user") user: UserSignInInput, @Ctx() ctx: IContext) {
    const { Login, Password } = user;

    const userentity = <UserEntity>(
      await GetUserByLoginAndPassword(Login, Password, ctx.db)
    );

    const { FirstName, LastName, DateOfBirth } = userentity;

    if (!userentity) throw new Error("Неверная пара логин - пароль");

    const instance = new AccessEntity();
    instance.User = userentity;
    const token = sign({ FirstName, LastName, DateOfBirth }, "my secret");
    instance.Token = token;

    await ctx.db.manager.save(instance);

    const uar = new UserAccessResponse();
    uar.Token = token;
    return uar;
  }

  @Mutation(() => User)
  async Register(@Arg("user") user: UserSignUpInput, @Ctx() ctx: IContext) {
    const instance = new UserEntity();
    instance.Login = user.Login;
    instance.Password = user.Password;
    instance.FirstName = user.FirstName;
    instance.LastName = user.LastName;
    instance.Avatar = user.Avatar;
    instance.DateOfBirth = user.DateOfBirth;
    instance.PrivateInfo = user.PrivateInfo;
    return await ctx.db.manager.save(instance);
  }
}
