import { IContext } from "../Types/GeneralTypes";
import { UserEntity } from "../Database/Entity/UserEntity";
import { Connection } from "typeorm";
import { UserRequestFriendsEntity } from "../Database/Entity/UserRequestFriendsEntity";
import { UserRequestStatuses } from "../config";
import { FriendEntity } from "../Database/Entity/FriendEntity";

export const CheckUserExistByLogin = async (
  connection: Connection,
  Login: string
) => {
  const m = connection.manager.getRepository(UserEntity);
  const f = await m.findOne({ Login });
  if (f) return true;
  return false;
};

export const GetUserByLoginAndPassword = async (
  Login: string,
  Password: string,
  connection: Connection
) => {
  const m = connection.manager.getRepository(UserEntity);
  const f = await m.findOne({ Login, Password });
  if (f) return f;
  return false;
};

export const AuthUserFind = async (ctx: IContext, entity) => {
  const m = ctx.db.manager.getRepository(entity);
  const f = await m.findOne(ctx.auth.user.Id);
  if (f) return { found: f, manager: m };
  return false;
};

export const AuthUserFindAndAction = async (
  ctx: IContext,
  entityClassName: string,
  instanceUpdate,
  action
) => {
  const f = await AuthUserFind(ctx, entityClassName);
  if (f) {
    instanceUpdate
      ? await f.manager[action](instanceUpdate)
      : await f.manager[action](f.found);
    return f.found;
  }

  throw new Error("Пользователь не найден");
};

export const AuthUserFindAndSave = async (
  ctx: IContext,
  entityClassName: string,
  instanceUpdate
) => await AuthUserFindAndAction(ctx, entityClassName, instanceUpdate, "save");

export const AuthUserFindAndDelete = async (
  ctx: IContext,
  entityClassName: string
) => await AuthUserFindAndAction(ctx, entityClassName, null, "delete");

export const existsRequest = async (Id: number, ctx: IContext) => {
  const request = await ctx.db.manager
    .createQueryBuilder(UserRequestFriendsEntity, "urf")
    .where("urf.UserId = :uid", { uid: ctx.auth.user.Id })
    .andWhere("urf.FriendId = :fid", { fid: Id })
    .leftJoinAndSelect("urf.Status", "status")
    .andWhere("NOT status.Id = :sid", {
      sid: UserRequestStatuses.rejected.id,
    })
    .getOne();
  if (request) return true;
  return false;
};

export const existsUser = async (Id: number, ctx: IContext) => {
  const user = await ctx.db.manager
    .createQueryBuilder(UserEntity, "u")
    .where("u.Id = :uid", { uid: Id })
    .getOne();
  if (user) return user;
  return false;
};

export const existsFriend = async (Id: number, ctx: IContext) => {
  const friend = await ctx.db.manager
    .createQueryBuilder(FriendEntity, "Friends")
    .leftJoinAndSelect("Friends.Friend", "Friend")
    .where("Friends.UserId = :uid", { uid: ctx.auth.user.Id })
    .andWhere("Friends.FriendId = :fid", { fid: Id })
    .getOne();
  if (friend) return true;
  return false;
};

export const isIamUser = (Id: number, ctx: IContext) => {
  return ctx.auth.user.Id === Id;
};

export const getInvite = async (IdFrind: number, ctx: IContext) => {
  const inviteQuery = ctx.db.manager
    .createQueryBuilder(UserRequestFriendsEntity, "UserRequestFriends")
    .where("UserRequestFriends.FriendId = :fid", { fid: IdFrind })
    .andWhere("UserRequestFriends.UserId = :uid", { uid: ctx.auth.user.Id })
    .andWhere("UserRequestFriends.StatusId = :sid", {
      sid: UserRequestStatuses.accepted.id,
    });

  let invite = await inviteQuery.getOne();

  if (invite)
    return {
      inviteQuery,
      action: async () => await inviteQuery.delete().execute(),
    };

  const inviteFriendQuery = ctx.db.manager
    .createQueryBuilder(UserRequestFriendsEntity, "UserRequestFriends")
    .where("UserRequestFriends.FriendId = :fid", { fid: ctx.auth.user.Id })
    .andWhere("UserRequestFriends.UserId = :uid", { uid: IdFrind })
    .andWhere("UserRequestFriends.StatusId = :sid", {
      sid: UserRequestStatuses.accepted.id,
    });

  let inviteFriend = await inviteFriendQuery.getOne();

  if (inviteFriend)
    return {
      inviteQuery: inviteFriendQuery,
      action: async () =>
        await inviteFriendQuery
          .update({
            StatusId: UserRequestStatuses.rejected.id,
          })
          .execute(),
    };
};
