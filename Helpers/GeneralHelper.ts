import { Connection } from "typeorm";

export const optionsGQL = (schema, connection:Connection, auth = {}, rootValue = {}) => {
  return {
    schema,
    rootValue,
    context: {
      auth,
      db: connection,
    },
    graphiql: true,
  };
};
