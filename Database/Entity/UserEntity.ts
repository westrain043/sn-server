import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  JoinColumn,
} from "typeorm";
import { FriendEntity } from "./FriendEntity";

export const table = "Users";

@Entity({ name: table })
export class UserEntity {
  @PrimaryGeneratedColumn()
  Id: number;
  @Column({ type: "varchar" })
  FirstName: string;
  @Column({ type: "varchar" })
  LastName: string;
  @Column({ type: "text", nullable: true })
  Avatar: string;
  @Column({ type: "date" })
  DateOfBirth: Date;
  @OneToMany(() => FriendEntity, (friend: FriendEntity) => friend.User, {
    onDelete: "CASCADE",
  })
  @JoinColumn({ name: "Id" })
  Friends: UserEntity[];
  @Column({ type: "text", nullable: true })
  PrivateInfo: string;
  @Column({ type: "varchar", unique: true })
  Login: string;
  //примитивно без криптографии
  @Column({ type: "varchar" })
  Password: string;
}
