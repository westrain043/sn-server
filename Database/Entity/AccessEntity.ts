import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  JoinColumn,
  OneToOne,
  ManyToOne,
} from "typeorm";
import { FriendEntity } from "./FriendEntity";
import { UserEntity } from "./UserEntity";

export const table = "Access";

@Entity({ name: table })
export class AccessEntity {
  @PrimaryGeneratedColumn()
  Id: number;
  @Column({ type: "text" })
  Token: string;
  @ManyToOne(() => UserEntity, (user: UserEntity) => user.Id, {
    onDelete: "CASCADE",
  })
  @JoinColumn({ name: "UserId" })
  User: UserEntity;
}
