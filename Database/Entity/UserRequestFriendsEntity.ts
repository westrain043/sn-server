import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
  AfterUpdate,
} from "typeorm";
import { UserRequestStatusEntity } from "./UserRequestStatusEntity";
import { UserEntity } from "./UserEntity";

export const table = "UserRequestFriends";

@Entity({ name: table })
export class UserRequestFriendsEntity {
  @PrimaryGeneratedColumn()
  Id: number;

  @ManyToOne(() => UserEntity, (user: UserEntity) => user.Id, {
    onDelete: "CASCADE",
  })
  @JoinColumn({ name: "UserId" })
  User: UserEntity;

  @ManyToOne(() => UserEntity, (user: UserEntity) => user.Id, {
    onDelete: "CASCADE",
  })
  @JoinColumn({ name: "FriendId" })
  Friend: UserEntity;

  @ManyToOne(
    () => UserRequestStatusEntity,
    (status: UserRequestStatusEntity) => status.Id,
    {
      onDelete: "CASCADE",
    }
  )
  @JoinColumn({ name: "StatusId" })
  Status: UserRequestStatusEntity;

  @Column({ type: "int" })
  UserId: number;

  @Column({ type: "int" })
  FriendId: number;

  @Column({ type: "int" })
  StatusId: number;
}
