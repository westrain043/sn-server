import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  JoinColumn,
} from "typeorm";
import { UserEntity } from "./UserEntity";

export const table = "Friends";

@Entity({ name: table })
export class FriendEntity {
  @PrimaryGeneratedColumn()
  Id: number;

  @ManyToOne(() => UserEntity, (user: UserEntity) => user.Id, {
    onDelete: "CASCADE",
  })
  @JoinColumn({ name: "UserId" })
  User: UserEntity;

  @ManyToOne(() => UserEntity, (user: UserEntity) => user.Id, {
    onDelete: "CASCADE",
  })
  @JoinColumn({ name: "FriendId" })
  Friend: UserEntity;

  @Column({ type: "int" })
  UserId: number;

  @Column({ type: "int" })
  FriendId: number;
}
