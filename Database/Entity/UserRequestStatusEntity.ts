import { Entity, Column, PrimaryColumn } from "typeorm";

export const table = "UserRequestStatus";

@Entity({ name: table })
export class UserRequestStatusEntity {
  @PrimaryColumn()
  Id: number;

  @Column({ type: "varchar", unique: true })
  Name: string;
}
