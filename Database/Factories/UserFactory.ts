import * as Faker from "faker";
import { define } from "typeorm-seeding";
import { UserEntity } from "../Entity/UserEntity";

import { usersAndFriends } from "./Data";

const data = [...usersAndFriends];

define(UserEntity, (faker: typeof Faker) => {
  const user = new UserEntity();
  user.FirstName = faker.name.firstName();
  user.LastName = faker.name.lastName();
  user.Avatar = faker.image.avatar();
  user.DateOfBirth = faker.date.between("01.01.1955", "01.01.2010");
  user.PrivateInfo = faker.lorem.text();
  user.Login = data.pop().login;
  user.Password = faker.lorem.word();
  return user;
});
