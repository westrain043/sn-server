const usersAndFriends: any = [
  { login: "sinior", u: 1, f: 2, s: 1 },
  { login: "jony", u: 2, f: 3, s: 2 },
  { login: "dog", u: 2, f: 4, s: 1 },
  { login: "aika", u: 2, f: 5, s: 2 },
  { login: "david", u: 3, f: 2, s: 1 },
  { login: "ivan", u: 3, f: 1, s: 3 },
  { login: "dodo", u: 5, f: 9, s: 3 },
  { login: "sanchez", u: 5, f: 7, s: 1 },
  { login: "pepe", u: 7, f: 2, s: 2 },
  { login: "ai", u: 10, f: 2, s: 2 },
];

usersAndFriends.sort();

const friends = [...usersAndFriends.filter((e) => e.s === 2)];

const friendsArr = friends.reduce((a, c) => {
  const u = c.f;
  const f = c.u;
  a.push({ u, f, s: c.s });
  return a;
}, friends);

export { usersAndFriends, friendsArr };
