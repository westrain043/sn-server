import * as Faker from "faker";
import { define } from "typeorm-seeding";
import { FriendEntity } from "../Entity/FriendEntity";
import {friendsArr} from "./Data";

const data = [...friendsArr];

define(FriendEntity, (faker: typeof Faker) => {
  if (data.length === 0) return;
  const ins = new FriendEntity();
  const i = faker.random.number({ min: 1, max: data.length - 1 });
  const element = data.pop();
  ins.UserId = element.u;
  ins.FriendId = element.f;
  return ins;
});

