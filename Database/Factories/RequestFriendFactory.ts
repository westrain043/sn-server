import * as Faker from "faker";
import { define } from "typeorm-seeding";
import { UserRequestFriendsEntity } from "../Entity/UserRequestFriendsEntity";
import {usersAndFriends} from "./Data";

const data = [...usersAndFriends];

define(UserRequestFriendsEntity, (faker: typeof Faker) => {
  const ins = new UserRequestFriendsEntity();
  const i = faker.random.number({ min: 1, max: data.length - 1 });
  const element = data.pop();
  ins.UserId = element.u;
  ins.FriendId = element.f;
  ins.StatusId = element.s;
  return ins;
});
