import * as moment from "moment";
import { createConnection, ConnectionOptions, Connection } from "typeorm";
import ormconfig = require("../ormconfig");

const OPTIONS = <ConnectionOptions>ormconfig;

export const db = async (
  resolveers = [],
  subscribers = [],
  synchronize = false
): Promise<Connection> => {
  return createConnection({
    ...OPTIONS,
    entities: [...resolveers],
    subscribers: [...subscribers],
    synchronize,
  })
    .then((connection) => {
      console.log(
        `[${moment().format(
          "DD.MM.YYYY hh:mm:ss"
        )}] Connection established with ${OPTIONS.type} database`
      );
      return connection;
    })
    .catch((err) => {
      console.error(
        `[${moment().format(
          "DD.MM.YYYY hh:mm:ss"
        )}] Error connection established with ${OPTIONS.type} database | ${
          err.message
        }`
      );

      process.exit();
    });
};
