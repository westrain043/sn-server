import { Seeder, Factory } from "typeorm-seeding";
import { UserEntity } from "../Entity/UserEntity";
import { FriendEntity } from "../Entity/FriendEntity";
import { UserRequestFriendsEntity } from "../Entity/UserRequestFriendsEntity";
import { friendsArr } from "../Factories/Data";

export default class All implements Seeder {
  public async run(factory: Factory): Promise<void> {
    await factory(UserEntity)({ roles: [] }).createMany(10);
    await factory(FriendEntity)({ roles: [] }).createMany(friendsArr.length);
    await factory(UserRequestFriendsEntity)({ roles: [] }).createMany(10);
  }
}
