import { database } from "./config";

export = {
  type: "mysql",
  ...database,
  entities: ["./Database/Entity/*.{js,ts}"],
  migrations: ["./Database/Migration/*.{js,ts}"],
  seeds: ["./Database/Seeds/*.{js,ts}"],
  factories: ["./Database/Factories/*.{js,ts}"],
  cli: {
    migrationsDir: "./Database/Migration",
  },
  synchronize: false,
  logging: false,
};
